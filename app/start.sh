#!/bin/bash

set -eu

export CONFDIR="/state/config"
export DBDIR="/state/db"

export KEYSDIR="${CONFDIR}/keys"
export PATTERNSDIR="${CONFDIR}/patterns"
export ZONESDIR="${CONFDIR}/zones"
export XFRDIR="${DBDIR}/xfr"

###############################################################################

export CONF="$CONFDIR/nsd.conf"
export ZONESINDEX="$CONFDIR/zones.index.conf"

mkdir -p "$ZONESDIR" "$KEYSDIR" "$PATTERNSDIR" "$XFRDIR"
chown -R nsd "$CONFDIR" "$DBDIR"



if [[ ! -e "$CONF" || -n "${NSD_FORCE:-}" ]]; then
	if [[ -z "${NSD_HOSTNAME:-}" ]]; then
		export NSD_HOSTNAME="$(hostname -f)"
		echo "using hostname $NSD_HOSTNAME as nsd identity" >&2
	fi

	echo "writing configuration" >&2
	envsubst </app/nsd.template.conf >"$CONF"
fi

echo "# auto-generated ... don't edit" >"$ZONESINDEX"

cd "$ZONESDIR"

while read FILE; do
	DOMAIN="${FILE##*/}"
	DOMAIN="${DOMAIN%.zone}"

	if [[ "${DOMAIN:0:1}" != "." ]]; then
		DOMAIN="$DOMAIN" FILE="$FILE" \
		envsubst </app/zone.index.template.conf >>"$ZONESINDEX"
	fi
done < <(find "$ZONESDIR" -type f -name "*.zone")

/usr/sbin/nsd -c "$CONF" -P /nsd.pid -d
