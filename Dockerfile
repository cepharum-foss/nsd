FROM alpine:latest

RUN apk add --no-cache tini bash nsd gettext
COPY app/ /app/

VOLUME /state/config
VOLUME /state/db

EXPOSE 53 53/udp

CMD [ "/sbin/tini", "/bin/ash", "/app/start.sh" ]
